$(document).ready(function () {

    $(document).on('click', '.btn-resize-grid', function () {
        var type = $(this).attr('data-type');

        switch (type) {
            case 'big-grid':
                adJustBigGrid();
                break;
            case 'medium-grid':
                adjustMediumGrid();
                break;
            case 'small-grid':
                adjustSmallGrid()
                break;
        }
    });



    $('.toggle-sidebar').on('click', function () {
        $('#sidebar').toggleClass('active');
    });



    function adjustMediumGrid() {
        var div = $('.dynamic-grid');
        $('div.resizable', div).removeClass('col-md-4');
        $('div.resizable', div).removeClass('col-lg-2');
        $('div.resizable', div).addClass('col-md-3');

        // ADJUST IMAGES //
        $('.img-thumbnail', div).css('height', '230px');
    }
    
    function adJustBigGrid() {
        var div = $('.dynamic-grid');
        $('div.resizable', div).removeClass('col-lg-2');
        $('div.resizable', div).removeClass('col-md-3');
        $('div.resizable', div).addClass('col-md-4')

        $('.img-thumbnail', div).css('height', '330px');
    }
    
    function adjustSmallGrid() {
        var div = $('.dynamic-grid');
        $('div.resizable', div).removeClass('col-lg-2');
        $('div.resizable', div).removeClass('col-md-3');
        $('div.resizable', div).addClass('col-md-4');
        $('div.resizable', div).addClass('col-lg-2');

        // ADJUST IMAGES //
        $('.img-thumbnail', div).css('height', '');
    }


});